package com.services;

import com.models.menulist.Restaurant;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

import java.util.List;
import java.util.concurrent.Future;

public class RestaurantList {
    private List<Restaurant> restaurants;


    public void addRestaurants(Restaurant restaurant) {
        this.restaurants.add(restaurant);
    }
    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @Async
    public Future<String> showAllRestaurants() {
        String allRestaurants = "";
        for (Restaurant restaurant:
                restaurants) {
            allRestaurants = allRestaurants + ""+restaurant.getRestaurantName()+","+
                    restaurant.getRestaurantLocation();
        }
        return new AsyncResult<String>(allRestaurants);
    }
}
