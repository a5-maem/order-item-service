package com.services;

import com.states.*;

public class OrderItemSystem {

    State noOrderYet;
    State chosenRestaurant;
    State chosenItem;
    State checkingOut;
    State finishedOrder;

    State currentState = noOrderYet;

    public OrderItemSystem(){
        noOrderYet = new NoOrderYet(this);
        chosenRestaurant = new ChosenRestaurant(this);
        chosenItem = new ChosenItem(this);
        checkingOut = new CheckingOut(this);
        finishedOrder = new FinishedOrder(this);
    }

    public void proceedToCheckout() {
        currentState.proceedToCheckout();
    }

    public void discardOrder(){
        currentState.discardOrder();
    }
    public State getNoOrderYet() {
        return noOrderYet;
    }

    public State getChosenRestaurant() {
        return chosenRestaurant;
    }

    public State getChosenItem() {
        return chosenItem;
    }

    public State getCheckingOut() {
        return checkingOut;
    }

    public State getFinishedOrder() {
        return finishedOrder;
    }

    public void setCurrentState(State state) {
        currentState = state;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void start() {

    }

}
