package com.models.checkout;

import com.models.menulist.MenuItem;

import java.util.List;

public class Order {
    private List<MenuItem> orders;
    private int totalPrice;

    public Order(List<MenuItem> orders) {
        this.orders = orders;
        this.totalPrice = calculateTotalPrice();
    }

    private int calculateTotalPrice() {
        int counter = 0;
        for (MenuItem menu:
             orders) {
            counter += menu.getHargaItem();
        }
        return counter;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public List<MenuItem> getOrders() {
        return orders;
    }
}
