package com.models.menulist;

import java.util.List;
import java.util.Calendar;

public class Restaurant {
    private String restaurantName;
    private String restaurantLocation;
    private int openHour;
    private int closeHour;
    private List<MenuItem> restaurantMenu;

    public Restaurant(String restaurantName, String restaurantLocation,
                      int openHour, int closeHour,
                      List<MenuItem>restaurantMenu) {

        this.restaurantName = restaurantName;
        this.restaurantLocation = restaurantLocation;
        this.openHour = openHour;
        this.closeHour = closeHour;
        this.restaurantMenu = restaurantMenu;

    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(String restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public List<MenuItem> getRestaurantMenu() {
        return restaurantMenu;
    }

    public void setRestaurantMenu(List<MenuItem> restaurantMenu) {
        this.restaurantMenu = restaurantMenu;
    }

    public int getOpenHour() {
        return openHour;
    }

    public void setOpenHour(int openHour) {
        this.openHour = openHour;
    }

    public int getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(int closeHour) {
        this.closeHour = closeHour;
    }

    public boolean isOpen() {
        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        return hour >= openHour && hour <= closeHour;

    }
}
