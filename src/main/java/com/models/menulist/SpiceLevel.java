package com.models.menulist;

public class SpiceLevel {
    private final static int NOT_SPICY = 0;
    private final static int LIL_SPICY = 1;
    private final static int MED_SPICY = 2;
    private final static int REAL_SPICY = 3;
    private final static int EXTRA_SPICY = 4;
    private final static int ARE_YOU_CRAZY = 5;

    private int state = NOT_SPICY;
    private String description;

    public SpiceLevel(int level) {
        if (level==1){
            state = LIL_SPICY;
            description = "Lil' bit spicy";
        } else if (level==2){
            state = MED_SPICY;
            description = "Kinda spicy";
        } else if (level==3){
            state = REAL_SPICY;
            description = "Definitely spicy";
        } else if (level==4){
            state = EXTRA_SPICY;
            description = "Hot damn!";
        } else if (level==5){
            state = ARE_YOU_CRAZY;
            description = "FREE PROMAAG FOR YOU!";
        } else {
            description = "weak.";
        }
    }

    public String getDescription() {
        return description;
    }

}
