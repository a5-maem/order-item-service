package com.states;

import com.services.OrderItemSystem;
import com.states.State;

public class CheckingOut implements State {

    OrderItemSystem system;

    public CheckingOut(OrderItemSystem system) {
        this.system = system;
    }

    @Override
    public void proceedToCheckout() {

    }

    @Override
    public void discardOrder() {

    }
}
