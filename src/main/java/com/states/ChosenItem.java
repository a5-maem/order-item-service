package com.states;

import com.services.OrderItemSystem;
import com.states.State;

public class ChosenItem implements State {

    OrderItemSystem system;

    public ChosenItem(OrderItemSystem system) {
        this.system = system;
    }

    @Override
    public void proceedToCheckout() {

    }

    @Override
    public void discardOrder() {

    }
}
