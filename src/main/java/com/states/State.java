package com.states;

public interface State {

    public void proceedToCheckout();
    public void discardOrder();
}
