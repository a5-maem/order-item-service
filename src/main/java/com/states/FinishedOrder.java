package com.states;

import com.services.OrderItemSystem;
import com.states.State;

public class FinishedOrder implements State {

    OrderItemSystem system;

    public FinishedOrder(OrderItemSystem system) {
        this.system = system;
    }

    @Override
    public void proceedToCheckout() {

    }

    @Override
    public void discardOrder() {

    }
}
