package com.states;

import com.services.OrderItemSystem;
import com.states.State;

public class ChosenRestaurant implements State {

    OrderItemSystem system;

    public ChosenRestaurant(OrderItemSystem system) {
        this.system = system;
    }

    @Override
    public void proceedToCheckout() {

    }

    @Override
    public void discardOrder() {

    }
}
