package com.states;

import com.services.OrderItemSystem;
import com.states.State;

public class NoOrderYet implements State {

    OrderItemSystem system;

    public NoOrderYet(OrderItemSystem system) {
        this.system = system;
    }
    @Override
    public void proceedToCheckout() {

    }

    @Override
    public void discardOrder() {

    }
}
